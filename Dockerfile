FROM alpine:3.12

WORKDIR docker-phpfpm-nginx

#ADD https://packages.whatwedo.ch/php-alpine.rsa.pub /etc/apk/keys/php-alpine.rsa.pub
#ADD https://dl.bintray.com/php-alpine/key/php-alpine.rsa.pub /etc/apk/keys/php-alpine.rsa.pub
ADD https://php.hernandev.com/key/php-alpine.rsa.pub /etc/apk/keys/php-alpine.rsa.pub

#COPY config/php-alpine.rsa.pub /etc/apk/keys/php-alpine.rsa.pub

# make sure you can use HTTPS
RUN apk --update add ca-certificates
RUN echo "https://php.hernandev.com/v3.11/php-7.4" >> /etc/apk/repositories


# Install packages
RUN apk --no-cache add php php-fpm php-opcache php-openssl php-curl \
	curl 
  #y
RUN apk add nginx supervisor curl python3 openrc

#RUN apk update && apk add -y --no-cache\
#  python-pip
#RUN apk add --update  py3-pip
RUN python3 -m ensurepip

RUN pip3 install --no-cache --upgrade pip setuptools
RUN pip3 install --no-cache-dir pytest-testinfra


# https://github.com/codecasts/php-alpine/issues/21
RUN ln -s /usr/bin/php7 /usr/bin/php

# Configure nginx
COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/test.py /etc/nginx/test.py

#ADD /home/alexbbell/Projects/Testinfra/docker-phpfpm-nginx/config/nginx.conf /etc/nginx/nginx.conf

# Remove default server definition
RUN rm /etc/nginx/conf.d/default.conf


# Create folder /etc/nginx/test
RUN mkdir /etc/nginx/test


#install supertools and testinfra
#RUN pip install --upgrade build
RUN pip install pytest-testinfra



# Configure PHP-FPM
COPY config/fpm-pool.conf /etc/php7/php-fpm.d/www.conf
COPY config/php.ini /etc/php7/conf.d/custom.ini

# Configure supervisord
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Setup document root
RUN mkdir -p /var/www/html

# Make sure files/folders needed by the processes are accessable when they run under the nobody user
RUN chown -R nobody.nobody /var/www/html && \
  chown -R nobody.nobody /run && \
  chown -R nobody.nobody /var/lib/nginx && \
  chown -R nobody.nobody /var/log/nginx

# Switch to use a non-root user from here on
USER nobody

# Add application
WORKDIR /var/www/html
COPY --chown=nobody src/ /var/www/html/

# Expose the port nginx is reachable on
EXPOSE 8080

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

# Configure a healthcheck to validate that everything is up&running
HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:8080/fpm-ping

RUN pytest -v /etc/nginx/test.py
