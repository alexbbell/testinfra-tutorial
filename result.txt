Sending build context to Docker daemon  27.14kB
Step 1/28 : FROM alpine:3.12
 ---> 24c8ece58a1a
Step 2/28 : WORKDIR docker-phpfpm-nginx
 ---> Using cache
 ---> a9c9cc1cd313
Step 3/28 : ADD https://php.hernandev.com/key/php-alpine.rsa.pub /etc/apk/keys/php-alpine.rsa.pub


 ---> Using cache
 ---> 4893c9b99e32
Step 4/28 : RUN apk --update add ca-certificates
 ---> Using cache
 ---> cb40a08ef4b2
Step 5/28 : RUN echo "https://php.hernandev.com/v3.11/php-7.4" >> /etc/apk/repositories
 ---> Using cache
 ---> ee233ff45a1b
Step 6/28 : RUN apk --no-cache add php php-fpm php-opcache php-openssl php-curl 	curl
 ---> Using cache
 ---> 66e59560ac06
Step 7/28 : RUN apk add nginx supervisor curl python3 openrc
 ---> Using cache
 ---> d03a8bff4aa4
Step 8/28 : RUN python3 -m ensurepip
 ---> Using cache
 ---> ece6e03db19e
Step 9/28 : RUN pip3 install --no-cache --upgrade pip setuptools
 ---> Using cache
 ---> bd2a14334b3d
Step 10/28 : RUN pip3 install --no-cache-dir pytest-testinfra
 ---> Using cache
 ---> 26f41dff7b27
Step 11/28 : RUN ln -s /usr/bin/php7 /usr/bin/php
 ---> Using cache
 ---> 42a5a697abf4
Step 12/28 : COPY config/nginx.conf /etc/nginx/nginx.conf
 ---> Using cache
 ---> da32347f8a33
Step 13/28 : COPY config/test.py /etc/nginx/test.py
 ---> Using cache
 ---> 6ae9b5bb07a0
Step 14/28 : RUN rm /etc/nginx/conf.d/default.conf
 ---> Using cache
 ---> a041782f3b00
Step 15/28 : RUN mkdir /etc/nginx/test
 ---> Using cache
 ---> 7ff7e07a4e10
Step 16/28 : RUN pip install pytest-testinfra
 ---> Using cache
 ---> befdf8cda65c
Step 17/28 : COPY config/fpm-pool.conf /etc/php7/php-fpm.d/www.conf
 ---> Using cache
 ---> 2249730765b9
Step 18/28 : COPY config/php.ini /etc/php7/conf.d/custom.ini
 ---> Using cache
 ---> 04c664b8a857
Step 19/28 : COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
 ---> Using cache
 ---> 8d0cfc36f131
Step 20/28 : RUN mkdir -p /var/www/html
 ---> Using cache
 ---> 91738726104b
Step 21/28 : RUN chown -R nobody.nobody /var/www/html &&   chown -R nobody.nobody /run &&   chown -R nobody.nobody /var/lib/nginx &&   chown -R nobody.nobody /var/log/nginx
 ---> Using cache
 ---> 44dc1817f8e0
Step 22/28 : USER nobody
 ---> Using cache
 ---> 76a23824644b
Step 23/28 : WORKDIR /var/www/html
 ---> Using cache
 ---> 183d4db22a20
Step 24/28 : COPY --chown=nobody src/ /var/www/html/
 ---> Using cache
 ---> 25cbd0cf43ab
Step 25/28 : EXPOSE 8080
 ---> Using cache
 ---> 7d55c27251b4
Step 26/28 : CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
 ---> Using cache
 ---> 89d9a5ed5cc6
Step 27/28 : HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:8080/fpm-ping
 ---> Using cache
 ---> 6ccbd220ea2c
Step 28/28 : RUN pytest -v /etc/nginx/test.py
 ---> Using cache
 ---> 8b4601541874
Successfully built 8b4601541874
Successfully tagged skillboxdocker:latest
