import unittest
import testinfra


def test_os(host):
    assert host.file("/etc/os-release").contains("Alpine")


def test_nginx_is_installed(host):
    nginx = host.package("nginx");
    assert nginx.is_installed
    assert nginx.version.startswith("1.1");
